package com.in28minutes.rest.webservices.restfulwebservices.users;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.in28minutes.rest.webservices.restfulwebservices.jpa.PostRepositoy;
import com.in28minutes.rest.webservices.restfulwebservices.jpa.UserRepositoy;

import jakarta.validation.Valid;

@RestController
public class UserJpaResource {
	
	
	@Autowired
	private UserRepositoy userRepositoy;
	
	@Autowired
	private PostRepositoy postRepositoy;
	
	public UserJpaResource(UserDaoService service,UserRepositoy userRepositoy,PostRepositoy postRepositoy) {
		super();
		this.postRepositoy=postRepositoy;
		this.userRepositoy=userRepositoy;
	}

	@GetMapping(path="/jpa/users")
	public List<User> retrieveAllUsers(){
		return userRepositoy.findAll();
	}
	//http://localhost:8080/users/1
	
	@GetMapping(path="/jpa/users/{id}")
	public EntityModel<User> retrieveUserById(@PathVariable Integer id){
		Optional<User> user =userRepositoy.findById(id);
		
		if(user.isEmpty()) {
			throw new UserNotFoundException("id "+id);
		}
		EntityModel<User> entityModel= EntityModel.of(user.get());
		
		WebMvcLinkBuilder link = linkTo(methodOn(this.getClass()).retrieveAllUsers());
		entityModel.add(link.withRel("all-users"));
		return entityModel;
	}
	
	@DeleteMapping(path="/jpa/users/{id}")
	public void deleteUserById(@PathVariable Integer id){
		userRepositoy.deleteById(id);
	}
	

	@GetMapping(path="/jpa/users/{id}/posts")
	public List<Post> retrievePostsForUser(@PathVariable Integer id){
	
		 Optional<User> user =userRepositoy.findById(id);
			
			if(user.isEmpty()) {
				throw new UserNotFoundException("id "+id);
			}
			return user.get().getPost();
	}
	
	@PostMapping("/jpa/users")
	public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
		User savedUser=userRepositoy.save(user);
		
		URI location=ServletUriComponentsBuilder.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(savedUser.getId())
					.toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PostMapping(path="/jpa/users/{id}/posts")
	public ResponseEntity<Object> createPostsForUser(@PathVariable Integer id, @Valid @RequestBody Post post ){
	
		 Optional<User> user =userRepositoy.findById(id);
			
			if(user.isEmpty()) {
				throw new UserNotFoundException("id "+id);
			}
			post.setUser(user.get());
			Post savedPost= postRepositoy.save(post);
			URI location=ServletUriComponentsBuilder.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(savedPost.getId())
					.toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	
	
	
	
	
	

}
