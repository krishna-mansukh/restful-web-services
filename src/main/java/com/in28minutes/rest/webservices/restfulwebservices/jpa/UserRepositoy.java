package com.in28minutes.rest.webservices.restfulwebservices.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.in28minutes.rest.webservices.restfulwebservices.users.User;

public interface UserRepositoy extends JpaRepository<User, Integer>{

}
