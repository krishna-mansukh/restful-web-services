package com.in28minutes.rest.webservices.restfulwebservices.users;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.stereotype.Component;

@Component
public class UserDaoService {
	
	

	private static int userCount=0;
	private static List<User> users= new ArrayList<>();
	
	static {
		users.add(new User(++userCount,"Adam",LocalDate.now().minusYears(30)));
		users.add(new User(++userCount,"Eve",LocalDate.now().minusYears(25)));
		users.add(new User(++userCount,"Jim",LocalDate.now().minusYears(20)));
		
	}
	

	
	public List<User> findAll(){
		return users;
	}
	
	public User findById(Integer id){
		Predicate<? super User> Predicate = user -> user.getId().equals(id);
		return users.stream().filter(Predicate).findFirst().orElse(null);
	}
	
	public void deleteById(Integer id){
		Predicate<? super User> Predicate = user -> user.getId().equals(id);
		users.removeIf(Predicate);
	}
	
	public User save(User user) {
		user.setId(++userCount);
		users.add(user);
		return user;
		
	}
	
	

}
